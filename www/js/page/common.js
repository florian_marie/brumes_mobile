$(".menu_pic").on("click",function(e){
    e.preventDefault();
    if($(this).hasClass("menu_pic_active"))
    {
        $('.swipe_menu').transition({width: '50px',height:'0px',opacity:0},500,'ease').transition({display:'none'},0);

        $(this).removeClass("menu_pic_active");
        $(this).find('.fa-times').removeClass('fa-times').addClass('fa-bars');
    }else
    {
        $('.swipe_menu').transition({display:'block',opacity:1},0).transition({width: '100%',height:'calc(100% - 50px)'},500,'ease');

        $(this).addClass("menu_pic_active");
        $(this).find('.fa-bars').removeClass('fa-bars').addClass('fa-times');
    }
});
