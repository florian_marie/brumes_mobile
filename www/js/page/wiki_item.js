$('.single_item').on('click swipe',function(){
    $item = $(this).parent();
    if($item.find('.single_arrow').hasClass('fa-active'))
    {
        /*$item.find('.single_item').removeClass('single_item_active').find('.single_arrow').removeClass('fa-active');*/
/*        $item.find('.content_item').velocity('transition.perspectiveDownOut', {
            duration: 300,
            stagger: 100,
            display: "none"
        });*/
        $item.find('.single_image').transition({width:'20%'});
        $item.find('.single_arrow').transition({width:"15%"});
        //setTimeout(function(){
            $item.find('.single_item').removeClass('single_item_active').find('.single_arrow').removeClass('fa-active');
        //},300);
        $item.find('.single_image img').velocity('transition.perspectiveDownIn', {
            duration: 300,
            stagger: 100,
            display: "block",
            complete: function(){


                $item.find('.content_item').velocity('transition.perspectiveDownOut', {
                    duration: 300,
                    stagger: 100,
                    display: "none"
                });



            }
        });

    }
    else
    {
        //$item.find('.single_item').addClass('single_item_active').find('.single_arrow').addClass('fa-active');
/*        $item.find('.content_item').velocity('transition.perspectiveDownIn', {
            duration: 300,
            stagger: 100,
            display: "block"
        });*/
        $item.find('.single_image').transition({width:'0px'});
        $item.find('.single_arrow').transition({width:'75px'});
        $item.find('.single_image img').velocity('transition.perspectiveUpOut', {
            duration: 300,
            stagger: 100,
            display: "none",
            complete: function(){

                $item.find('.content_item').velocity('transition.perspectiveDownIn', {
                    duration: 300,
                    stagger: 100,
                    display: "block"
                });

                setTimeout(function(){
                    $item.find('.single_item').addClass('single_item_active').find('.single_arrow').addClass('fa-active');
                },300);

            }
        });
    }
});