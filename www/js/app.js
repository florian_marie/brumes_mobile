$api_base = "http://api.brumes.eu";
$debug = true;
$before = null;
$targetId = null;
document.addEventListener("pageinit", function (e) {
    $targetId = e.target.id;
    if (e.target.id == "login") {
        inc("login");
    }
    else if (e.target.id == "home") {
        inc("home");
    }
    else if (e.target.id == "news") {
        inc("news");
    }
    else if (e.target.id == "wiki") {
        inc("wiki");
    }
    else if (e.target.id == "wiki_item") {
        inc("wiki_item");
    }
    else if (e.target.id == "profil") {
        inc("profil");
    }
    try{

    }catch(Exc)
    {

    }
    inc("common");
}, false);

function inc(name) {
    $.getScript("js/page/" + name + ".js");
}



app.run(function ($rootScope) {
    window.localStorage.setItem("news", null);
    $.ajax({
        type: "GET",
        url: $api_base + "/news",
        dataType: "json",
        success: function (XMLHttpRequest) {
            $rootScope.news = XMLHttpRequest.success.content;
            $.each($rootScope.news,function($i,$e){
                try{
                    if(!$e.featured_image.source){
                        $.ajaxSetup({async: false});
                        $.get($rootScope.news[$i]["link"],function($html){
                            $rootScope.news[$i]["featured_image"]["source"] = $(".attachment-post-thumbnail",$html).attr("src");
                            $('ons-carousel-item[name='+$rootScope.news[$i]["ID"]+']').css("background-image","url('" + $rootScope.news[$i]["featured_image"]["source"] + "')");
                            $rootScope.$apply();
                        });
                        $.ajaxSetup({async: true});
                    }
                }catch(e)
                {
                }
            });
            window.localStorage.setItem("news", JSON.stringify($rootScope.news));
            $rootScope.$apply();

        },
        error: function (XMLHttpRequest) {
            $rootScope.news = null;
            $rootScope.$apply();
        }
    });


    if (typeof $itemLoaded == "undefined") {
        $itemLoaded = [];
    }
    if (typeof $itemLoaded != "undefined" && $itemLoaded != null)  // il y a des maj a faire
    {
        if ($itemLoaded.length == 0) {
            $.ajax({
                type: "GET",
                url: $api_base + "/stats/item",
                dataType: "json",
                data: {},
                success: function (XMLHttpRequest) {
                    $content = XMLHttpRequest.success.content;
                    $.each($content.types, function (i, d) {
                        $itemLoaded[i] = d;
                        $rootScope.items = $itemLoaded;
                        $content = window.localStorage.getItem("item." + i);
                        if ($content !== null) {
                            $cP = JSON.parse($content);
                            $cPd = new Date($cP["updated"]);
                            $nUp = new Date(d.updated);
                            if($nUp > $cPd)
                            {
                                $.ajax({
                                    type: "GET",
                                    url: d.url,
                                    dataType: "json",
                                    data: {},
                                    success: function (XMLHttpRequest) {
                                        window.localStorage.setItem("item." + i, JSON.stringify(XMLHttpRequest.success.content));
                                    },
                                    error: function (XMLHttpRequest) {
                                    }
                                });
                            }
                        } else {
                            $.ajax({
                                type: "GET",
                                url: d.url,
                                dataType: "json",
                                data: {},
                                success: function (XMLHttpRequest) {
                                    window.localStorage.setItem("item." + i, JSON.stringify(XMLHttpRequest.success.content));
                                },
                                error: function (XMLHttpRequest) {
                                }
                            });
                        }
                    });
                },
                error: function (XMLHttpRequest) {
                }
            });
        }

    }

});

app.controller("LoginCtrl", ['$scope', '$rootScope', function ($scope, $rootScope) {
    $scope.init = function () {
        $username = window.localStorage.getItem("user.username");
        $password = window.localStorage.getItem("user.password");

        $ipassword = $('input[name="login_password"]');
        $iusername = $('input[name="login_username"]');
        $ilocal = $('input[name="login_local"]');
        if ($username != null && $password != null) {
            $('.form').css('display', 'none');
            $ilocal.val(1);
            $iusername.val($username);
            $ipassword.attr('type', 'password').val($password);
            this.submit();
        }
    };

    $scope.submit = function () {
        /*if($debug){
         $username.val("Ezythas");
         $password.val("Bow4ys3ac0");
         }*/
        $username = $('input[name="login_username"]');
        $password = $('input[name="login_password"]');
        $pass = null;
        if ($('input[name="login_local"]').val() == 0)
            $pass = $.sha256($password.val());
        else
            $pass = $password.val();
            $.ajax({
            type: "POST",
            url: $api_base + "/auth",
            dataType: "json",
            data: {
                username: $username.val(),
                password: $pass
            },
            success: function (XMLHttpRequest) {
                $rootScope.user = XMLHttpRequest.success.content;
                if ($('input[name="login_local"]').val() == 0) {
                    window.localStorage.setItem('user.username', $username.val());
                    window.localStorage.setItem('user.password', $.sha256($password.val()));
                }

                if ($rootScope.user.characters.length > 0)
                    $rootScope.character = $rootScope.user.characters[0];
                while(window.localStorage.getItem("news") == null){}
                $rootScope.$apply();
                $scope.$apply();
                menu.setMainPage('home.html', {closeMenu: true});



            },
            error: function (XMLHttpRequest) {
                if ($('input[name="login_local"]').val() === 1) {
                    $('input[name="login_local"]').val(0);
                    $('.form').css('display', 'block');
                }

                $message = XMLHttpRequest.responseJSON.error.message;
                $error.html($message);
                $error.velocity('transition.flipBounceYIn', {
                    duration: 1000,
                    stagger: 100,
                    display: "block"
                });
            }
        });
    }

}]);


app.controller("HomeCtrl", ['$scope', '$rootScope', function ($scope, $rootScope) {
    $scope.chooseCharacter = function () {
        $popup = $('.homeChooseCharacter');
        if ($popup.css('display') == "none") {
            $popup.velocity('transition.slideDownIn', {
                duration: 300,
                stagger: 100,
                display: "block"
            });
        } else {
            $popup.velocity('transition.slideUpOut', {
                duration: 300,
                stagger: 100,
                display: "none"
            });
        }
    };
    $scope.goToArticle = function($id){
        $.each($rootScope.news,function(i,d) {
            if(d.ID == $id) {
                $rootScope.article = d;
                $before = "home.html";
                menu.setMainPage("article.html", {closeMenu: true});
            }
        });
    };

    $scope.goToNews = function(){
        $before = "home.html";
        menu.setMainPage('news.html', {closeMenu: true});
    };

    $scope.goToWiki = function(){
        $before = "home.html";
        menu.setMainPage('wiki.html', {closeMenu: true});
    };

    $scope.goToProfil = function(){
        $before = "home.html";
        menu.setMainPage('profil.html', {closeMenu: true});
    }
}]);


app.controller("NewsCtrl", ['$scope', '$rootScope', function ($scope, $rootScope) {
    $scope.goToHome = function(){
        menu.setMainPage('home.html', {closeMenu: true});
    };
    $scope.goToNews = function($id)
    {
        $.each($rootScope.news,function(i,d) {
            if(d.ID == $id) {
                $rootScope.article = d;
                $before = "news.html";
                menu.setMainPage("article.html", {closeMenu: true});
            }
        });
    };

    $scope.onRefresh = function()
    {
        $(".fa-refresh").addClass("fa-refresh-active");
        $.ajax({
            type: "GET",
            url: $api_base + "/news",
            dataType: "json",
            success: function (XMLHttpRequest) {
                $rootScope.news = XMLHttpRequest.success.content;
                $.each($rootScope.news,function($i,$e){
                    try{
                        if(!$e.featured_image.source){
                            $.ajaxSetup({async: false});
                            $.get($rootScope.news[$i]["link"],function($html){
                                $rootScope.news[$i]["featured_image"]["source"] = $(".attachment-post-thumbnail",$html).attr("src");
                                $('ons-carousel-item[name='+$rootScope.news[$i]["ID"]+']').css("background-image","url('" + $rootScope.news[$i]["featured_image"]["source"] + "')");
                                $rootScope.$apply();
                            });
                            $.ajaxSetup({async: true});
                        }
                    }catch(e)
                    {
                    }
                });
                window.localStorage.setItem("news", JSON.stringify($rootScope.news));
                $rootScope.$apply();
                $(".fa-refresh").removeClass("fa-refresh-active");
            },
            error: function (XMLHttpRequest) {
                $rootScope.news = null;
                $rootScope.$apply();
                $(".fa-refresh").removeClass("fa-refresh-active");
            }
        });
    }
}]);

app.controller("ArticleCtrl", ['$scope', '$rootScope', function ($scope, $rootScope) {
    $scope.goTo = function(){
        $b = $before;
        $before = "article.html";
        menu.setMainPage($b, {closeMenu: true});
    };
}]);

app.controller("ProfilCtrl", ['$scope', '$rootScope', function ($scope, $rootScope) {
    $scope.chooseCharacter = function () {
        $popup = $('.homeChooseCharacter');
        if ($popup.css('display') == "none") {
            $popup.velocity('transition.slideDownIn', {
                duration: 300,
                stagger: 100,
                display: "block"
            });
        } else {
            $popup.velocity('transition.slideUpOut', {
                duration: 300,
                stagger: 100,
                display: "none"
            });
        }
    };
}]);

app.controller("HomeCharacterCtrl", ['$scope', '$rootScope', function ($scope, $rootScope) {

    $scope.chooseCharacter = function (character) {
        $rootScope.character = character;
        $rootScope.$apply();
        $('.homeChooseCharacter').velocity('transition.slideUpOut', {
            duration: 1000,
            stagger: 100,
            display: "none"
        });
    }
}]);

app.controller("WikiCtrl", ['$scope', '$rootScope', function ($scope, $rootScope) {

    $scope.goToWikiItem = function($wiki)
    {
        if(window.localStorage.getItem("item." + $wiki) == null)
        {
            $.ajax({
                type: "GET",
                url: $api_base + "/item/" + $wiki,
                dataType: "json",
                data: {},
                success: function (XMLHttpRequest) {
                    window.localStorage.setItem("item." + $wiki, JSON.stringify(XMLHttpRequest.success.content));
                    $rootScope.wiki_item = JSON.parse(window.localStorage.getItem("item." + $wiki));
                    $rootScope.wiki_item.type = $wiki;
                    menu.setMainPage("wiki_item.html", {closeMenu: true});
                },
                error: function (XMLHttpRequest) {
                }
            });
        }else{
            $rootScope.wiki_item = JSON.parse(window.localStorage.getItem("item." + $wiki));
            $rootScope.wiki_item.type = $wiki;
            menu.setMainPage("wiki_item.html", {closeMenu: true});
        }

    };
}]);

app.controller("WikiItemCtrl", ['$scope', '$rootScope', function ($scope, $rootScope) {

}]);

app.controller("SwipeMenuCtrl", ['$scope', '$rootScope', function ($scope, $rootScope) {
    $scope.goTo = function(page)
    {
        $before = $targetId + ".html";
        menu.setMainPage(page, {closeMenu: true});
    }
}]);

app.filter('htmlToPlaintext', function () {
    return function (text) {
        return String(text).replace(/<[^>]+>/gm, '');
    };
});

app.filter('isArray', function() {
    return function (input) {
        return angular.isArray(input);
    };
});