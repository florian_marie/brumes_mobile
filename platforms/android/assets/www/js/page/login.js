$username = $('input[name="login_username"]');
$password = $('input[name="login_password"]');
$error = $(".form .login_error");
$submit = $('input[name="login_submit"]');

$.getScript("js/lib/jquery.sha256.min.js");
$.getScript("js/lib/velocity.js");

$submit.on('click',function(e){
    $.ajax({
        type: "POST",
        url: $api_base +"/auth",
        data: {
            username: $username.val(),
            password: $.sha256($password.val())
        },
        success: function(msg){
            menu.setMainPage('home.html', {closeMenu: true});
        },
        error: function(XMLHttpRequest) {
            $message = XMLHttpRequest.responseJSON.error.message;
            $error.html($message);
            $error.velocity('transition.flipBounceYIn', {
                duration: 1000,
                stagger: 100,
                display: "block"
            });
        }
    });

});

$password.on("focus",function(e){
   $(this).attr("type","password");
});